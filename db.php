<?php
 function connectDb() {
     // создаём подключение к БД
    $db = new mysqli("localhost", "root", "", "roboapp");

    // Check connection
    if($db === false){        
        die("ERROR: Не удалось подключиться к БД. " . mysqli_connect_error());
    }
    
    // Print host information
    #echo "Connect Successfully. Host info: " . mysqli_get_host_info($db) . "<br>";

    mysqli_query($db, "SET NAMES utf8");
    mysqli_query($db, "SET CHARACTER SET utf8");

    return $db;
 }
?>
