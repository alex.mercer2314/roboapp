<?php
include 'db.php';                                                                                                                   // подключаем файл с настройками подключения к БД

/**
 * метод получения всех сотрудников
 */
function getAllUsers() {                                                                                                            // метод получения всех сотрудников
    $data = array();                                                                                                                // объявляем результирующий массив
    $db = connectDb();                                                                                                              // подключаемся к БД

    $sql = 'select 
	    concat(user.last_name, " ", user.first_name, " ", user.middle_name) AS fullname,
	    position.name as position_name,
        user.created_at as user_hired,
        user_dismission.created_at as user_fired,
        dismission_reason.description as user_fired_reason,
        position.salary as user_salary,
        concat(user_leader.last_name, " ", user_leader.first_name, " ", user_leader.middle_name) AS leader_fullname
    
        from department
        JOIN user_position on (department.id = user_position.department_id)
        JOIN user on (user.id = user_position.user_id)
        JOIN user as user_leader on (user_leader.id = department.leader_id)
        JOIN position on (user_position.position_id = position.id) 
        LEFT JOIN user_dismission on (user_dismission.user_id = user.id) 
        LEFT JOIN dismission_reason on (user_dismission.reason_id = dismission_reason.id)';                                         // делаем выборку из базы

    $res = $db->query($sql);                                                                                                        // отправляем запрос в базу на получение данных

    while($row = $res->fetch_assoc()){                                                                                              // преобразовываем данные в ассоциативный массив
        $data[] = $row;
    }

    return $data;                                                                                                                   // возвращаем результирующий массив
}

/**
 * метод получения уволенных сотрудников
 */
function getFiredUsers() {                                                                                                          // метод получения уволенных сотрудников
    $data = array();                                                                                                                // объявляем результирующий массив
    $db = connectDb();                                                                                                              // подключаемся к БД

    $sql = 'select 
	    concat(user.last_name, " ", user.first_name, " ", user.middle_name) AS fullname,
	    position.name as position_name,
        user.created_at as user_hired,
        user_dismission.created_at as user_fired,
        dismission_reason.description as user_fired_reason,
        position.salary as user_salary,
        concat(user_leader.last_name, " ", user_leader.first_name, " ", user_leader.middle_name) AS leader_fullname
    
        from department
        JOIN user_position on (department.id = user_position.department_id)
        JOIN user on (user.id = user_position.user_id)
        JOIN user as user_leader on (user_leader.id = department.leader_id)
        JOIN position on (user_position.position_id = position.id) 
        LEFT JOIN user_dismission on (user_dismission.user_id = user.id) 
        LEFT JOIN dismission_reason on (user_dismission.reason_id = dismission_reason.id) 
        where user_dismission.created_at';                                                                                          // делаем выборку из базы по условию что у сотрудника есть запись об увольнении

    $res = $db->query($sql);                                                                                                        // отправляем запрос в базу на получение данных

    while($row = $res->fetch_assoc()){                                                                                              // преобразовываем данные в ассоциативный массив
        $data[] = $row;
    }

    return $data;                                                                                                                   // возвращаем результирующий массив
}


/**
 * метод получения сотрудников с испытательным сроком
 */
function getTempUsers() {                                                                                                           // метод получения сотрудников с испытательным сроком
    $data = array();                                                                                                                // объявляем результирующий массив
    $db = connectDb();                                                                                                              // подключаемся к БД


                                                       

    $sql = 'select 
    concat(user.last_name, " ", user.first_name, " ", user.middle_name) AS fullname,
    position.name as position_name,
    user.created_at as user_hired,
    user_dismission.created_at as user_fired,
    dismission_reason.description as user_fired_reason,
    position.salary as user_salary,
    concat(user_leader.last_name, " ", user_leader.first_name, " ", user_leader.middle_name) AS leader_fullname

    from department
    JOIN user_position on (department.id = user_position.department_id)
    JOIN user on (user.id = user_position.user_id)
    JOIN user as user_leader on (user_leader.id = department.leader_id)
    JOIN position on (user_position.position_id = position.id) 
    LEFT JOIN user_dismission on (user_dismission.user_id = user.id) 
    LEFT JOIN dismission_reason on (user_dismission.reason_id = dismission_reason.id) 
    where DATE_ADD(user.created_at, INTERVAL +3 MONTH) >= now()';                                                                   // делаем выборку из базы по условию что дата создания записи сотрудника + 3 месяца больше текущей даты

    $res = $db->query($sql);                                                                                                        // отправляем запрос в базу на получение данных

    while($row = $res->fetch_assoc()){                                                                                              // преобразовываем данные в ассоциативный массив
        $data[] = $row;
    }

    return $data;                                                                                                                   // возвращаем результирующий массив
}




/**
 * последний устроенный у начальника
 */
function getLastInDepartment() {                                                                                                    // метод получения сотрудников с испытательным сроком
    $data = array();                                                                                                                // объявляем результирующий массив
    $departmentList = array();                                                                                                      // объявляем результирующий массив для хранения списка ID департаментов
    $db = connectDb();                                                                                                              // подключаемся к БД

    $departmentSql = 'select id from department';                                                                                   // делаем выборку ID департаментов 

    $departmentResult = $db->query($departmentSql);                                                                                 // получаем данные по ID департаментов

    while($row = $departmentResult->fetch_assoc()){                                                                                 // преобразовываем данные в ассоциативный массив и делаем выборку по последнему устроенному сотруднику у начальника в данном департаменте по ID
        $departmentId = $row["id"];                                                                                                 // честно говоря, пытался сделать по-другому, но так как в голову ничего не пришло сделал через цикл
                                                                                                                                    
        $sql = '
        SELECT 
            concat(user.last_name, " ", user.first_name, " ", user.middle_name) AS fullname,
	        position.name as position_name,
            user.created_at as user_hired,
            user_dismission.created_at as user_fired,
            dismission_reason.description as user_fired_reason,
            position.salary as user_salary,
            concat(user_leader.last_name, " ", user_leader.first_name, " ", user_leader.middle_name) AS leader_fullname,
            user_position.department_id as department_id,
            department.name as name
        
        from user
        JOIN user_position on (user.id = user_position.user_id)
        join department on (department.id = user_position.department_id)
        join position on (position.id = user_position.position_id)
        JOIN user as user_leader on (user_leader.id = department.leader_id)
        LEFT JOIN user_dismission on (user_dismission.user_id = user.id) 
        LEFT JOIN dismission_reason on (user_dismission.reason_id = dismission_reason.id) 
        where department.leader_id in (
            SELECT 
            user.id
            from department
            JOIN user_position on (department.id = user_position.department_id)
            JOIN user on (user.id = user_position.user_id)
            JOIN user as user_leader on (user_leader.id = department.leader_id)
        )
            and user.id NOT IN (SELECT user_id FROM user_dismission) and department.id = '. $departmentId . '
        ORDER BY user.created_at DESC, user.id DESC
        LIMIT 1
        ';

        $res = $db->query($sql);                                                                                            // отправляем запрос в базу на получение данных    

        while($resRow = $res->fetch_assoc()){                                                                               // преобразовываем данные в ассоциативный массив
            $data[] = $resRow;
         }

    }

    return $data;                                                                                                           // возвращаем результирующий массив

}


function getFilteredData() {                                                                                                // метод для обработки запроса с фронта и формирования JSON
    $filter = $_REQUEST["filter"];                                                                                          // получаем id фильтра по сотрудникам
    $data = array();                                                                                                        // объявляем результирующий массив

    /**
     * 0 - все сотрудники
     * 1 - с испытательным сроком
     * 2 - уволенные
     * 3 - начальники
     */
   
    if($filter == 1) {                                                                                                      // проверяем код фильтра и вызываем метод для получения данных
        # испытательный срок
        $data = getTempUsers();    
    } elseif ($filter == 2) {
        # уволенные 
        $data = getFiredUsers();
    } elseif ($filter == 3) {    
        # последний нанятый 
        $data = getLastInDepartment();
    } else {
        # все сотрудники
        $data = getAllUsers();
    }

    echo json_encode($data);                                                                                                // выводим полученные данные на странице php для получения на фронте в виде json 
}

getFilteredData(); 
