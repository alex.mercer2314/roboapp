<?php

function getCsvData($tableName) {
    $data = array();                                                // создаём массив для хранения данных из файла
    $handle = fopen($tableName, "r");                               // открываем файл для чтения

    while(($fileData = fgetcsv($handle, 0, ";")) !== FALSE) {       // в цикле перебираем строки из файла и сохраняем их в массив
        $data[] = $fileData;
    }

    fclose($handle);                                                // закрываем файл

    return $data;                                                   // возвращаем данные из файла
}

/*
*   Метод импорта сотрудников из файла
*
*
**/
function importUsers() {

    $data = getCsvData("db/testdb_user.csv");                           // получаем данные из таблицы
                             
    $db = connectDb();                                                  // подключение к ДБ
    $sql = "insert into user (`id`, `first_name`, `last_name`, `middle_name`, `data_of_birth`, `created_at`, `updated_at`) values";    // sql-запрос для сохранения данных
    $values = "";                                                                                                               // переменная для сохранения распарсенных строк из csv

    foreach($data as $row) {                                           // перебираем данные для сохранения в бд

        $id = $row[0];
        $first_name = $row[1];                                         // Имя сотрудника
        $last_name = $row[2];                                          // Фамилия сотрудника
        $middle_name = $row[3];                                        // Отчество сотрудника
        $date_of_birth = date('Y-m-d',strtotime($row[4]));             // Дата рождения
        $created = date('Y-m-d H:i:s',strtotime($row[5]));             // устроен
        $updated = date('Y-m-d H:i:s',strtotime($row[6]));             // обновлен

        $values .= "('$id', '$first_name', '$last_name', '$middle_name', '$date_of_birth', '$created', '$updated'),";  // строка с данными сотрудника
    }

    $sql .=  $values;                                                   // добавляем склеенные данные по сотрудникам в запрос
	$res_sql = substr($sql,0,-1);                                       // сохраняем данные в результирующую строку
	$db->query($res_sql);                                               // вызываем метод для сохранения данных о сотрудниках


    if ($db->errno) {                                                   // на случай ошибки выводим данные о ней
        die('Select Error (' . $db->errno . ') ' . $db->error);
    }

    $db->close();                                                       // закрываем соединение с базой
}

/**
 * импорт департамента
 * id;leader_id;name;description;created_ad;update_at
 */
function importDepartment() {
    $data = getCsvData("db/testdb_department.csv");

    $db = connectDb();

    $sql = "insert into department (`id`, `leader_id`, `name`, `description`, `created_at`, `updated_at`) values";    // sql-запрос для сохранения данных
    $values = "";                                                                                        // переменная для сохранения распарсенных строк из csv

    foreach($data as $row) {
        $id = $row[0];
        $leader_id = $row[1];
        $name = $row[2];
        $descr = $row[3];
        $created = $row[4];
        $updated = $row[5];

        $values .= "('$id', '$leader_id', '$name', '$descr', '$created', '$updated'),";
    }

    $sql .= $values;
    $res_sql = substr($sql,0,-1);

    echo $res_sql;

    $db->query($res_sql); 

    if ($db->errno) {                                                   // на случай ошибки выводим данные о ней
        die('Select Error (' . $db->errno . ') ' . $db->error);
    }

    $db->close();                                                       // закрываем соединение с базой

}

function importDismissionReason() {
    $data = getCsvData("db/testdb_dismission_reason.csv");

    $db = connectDb();

    $sql = "insert into dismission_reason (`id`, `name`, `description`, `created_at`, `updated_at`) values";    // sql-запрос для сохранения данных
    $values = "";                                                                                        // переменная для сохранения распарсенных строк из csv

    foreach($data as $row) {
        $id = $row[0];
        $name = $row[1];
        $descr = $row[2];
        $created = $row[3];
        $updated = $row[4];

        $values .= "('$id', '$name', '$descr', '$created', '$updated'),";
    }

    $sql .= $values;
    $res_sql = substr($sql,0,-1);

    $db->query($res_sql); 

    if ($db->errno) {                                                   // на случай ошибки выводим данные о ней
        die('Select Error (' . $db->errno . ') ' . $db->error);
    }

    $db->close();                                                       // закрываем соединение с базой
}

function importPosition() {
    $data = getCsvData("db/testdb_position.csv");

    $db = connectDb();

    $sql = "insert into position (`id`, `name`, `description`, `salary`,  `created_at`, `updated_at`, `is_active`) values";    // sql-запрос для сохранения данных
    $values = "";                                                                                        // переменная для сохранения распарсенных строк из csv

    foreach($data as $row) {
        $id = $row[0];
        $name = $row[1];
        $descr = $row[2];
        $salary = $row[3];
        $created = $row[4];
        $updated = $row[5];
        $active = $row[6];

        $values .= "('$id', '$name', '$descr', '$salary', '$created', '$updated', '$active'),";
    }

    $sql .= $values;
    $res_sql = substr($sql,0,-1);

    $db->query($res_sql); 

    if ($db->errno) {                                                   // на случай ошибки выводим данные о ней
        die('Select Error (' . $db->errno . ') ' . $db->error);
    }

    $db->close();                                                       // закрываем соединение с базой
}

function importUserDismission() {
    $data = getCsvData("db/testdb_user_dismission.csv");

    $db = connectDb();

    $sql = "insert into user_dismission (`id`, `user_id`, `reason_id`, `is_active`,  `created_at`, `updated_at`) values";    // sql-запрос для сохранения данных
    $values = "";                                                                                        // переменная для сохранения распарсенных строк из csv

    foreach($data as $row) {
        $id = $row[0];
        $user_id = $row[1];
        $reason_id = $row[2];
        $active = $row[3];
        $created = $row[4];
        $updated = $row[5];
    
        $values .= "('$id', '$user_id', '$reason_id', '$active', '$created', '$updated'),";
    }

    $sql .= $values;
    $res_sql = substr($sql,0,-1);

    $db->query($res_sql); 

    if ($db->errno) {                                                   // на случай ошибки выводим данные о ней
        die('Select Error (' . $db->errno . ') ' . $db->error);
    }

    $db->close();                                                       // закрываем соединение с базой
}

function importUserPosition() {
    $data = getCsvData("db/testdb_user_position.csv");

    $db = connectDb();

    $sql = "insert into user_position (`id`, `user_id`, `department_id`, `position_id`,  `created_at`, `updated_at`) values";    // sql-запрос для сохранения данных
    $values = "";                                                                                        // переменная для сохранения распарсенных строк из csv

    foreach($data as $row) {
        $id = $row[0];
        $user_id = $row[1];
        $department_id = $row[2];
        $position_id = $row[3];
        $created = $row[4];
        $updated = $row[5];
    
        $values .= "('$id', '$user_id', '$department_id', '$position_id', '$created', '$updated'),";
    }

    $sql .= $values;
    $res_sql = substr($sql,0,-1);

    $db->query($res_sql); 

    if ($db->errno) {                                                   // на случай ошибки выводим данные о ней
        die('Select Error (' . $db->errno . ') ' . $db->error);
    }

    $db->close();                                                       // закрываем соединение с базой
}

function fillTablesDB() {
    ### import users 
    importUsers();                           // вызов метода для сохранения таблицы user в БД (выполнен)

    ### import department 
    importDepartment();                     // (выполнен)

    ### import dismission_reason
    importDismissionReason();               // выполнен

    ### import position 
    importPosition();                       // выполнен

    ### import user_dismission
    importUserDismission();                 // выполнен

    ### import user_position
    importUserPosition();                   // выполнен
}
