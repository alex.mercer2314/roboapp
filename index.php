<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    
    <title>Тестовое задание</title>
    
</head>
<body>

    <div class="container">
        <h4 class="table-title">Таблица сотрудников</h4>
        
        <form class="button-group">
            <input type="radio" id="all" name="filter" value="all" onClick="sendAjax()" checked><label for="all">Все</label></input>
            <input type="radio" id="temp" name="filter" value="temp" onClick="sendAjax(1)"><label for="temp">Испытательный срок</label></input>
            <input type="radio" id="fired" name="filter" value="fired" onClick="sendAjax(2)"><label for="fired">Уволенные</label></input>
            <input type="radio" id="chief" name="filter" value="chief" onClick="sendAjax(3)"><label for="chief">Начальники</label></input>
        </form>

        <table class="table table-striped">
            <thead>
                <tr class="table-primary">
                    <td>ФИО</td>
                    <td>Должность</td>
                    <td>Дата приема</td>
                    <td>Дата увольнения</td>
                    <td>Причина увольнения</td>
                    <td>Размер ЗП</td>
                    <td>Начальник</td>
                </tr>                
            </thead>
            <tbody id="table-view">
            
            </tbody>
        </table>
        <div class="footer">
            <div class="label">Страница: </div>
            <div class="pagination"></div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script type="text/javascript">

        const tableStrings = 10;                                                                                            // задаём кол-во записей на странице
        let activePage = 1;                                                                                                 // индекс активной страницы
        let tableData = [];                                                                                                 // массив для хранения данных 

        function createPagination(pageCount = 0) {                                                                          // функция создания пагинации
            console.log('Вызов метода построения пагинации');
            let pageView = document.getElementsByClassName("pagination")[0];                                                // задаём контейнер хранения пагинации

            pageView.innerHTML = "";                                                                                        // очищаем контейнер

            for(let i = 1; i <= pageCount; i++) {                                                                           // в цикле создаём элемент кнопки по количеству страниц в таблице
                pageView.innerHTML += `<div class="page" onClick="setActivePage(${i})">${i}</div>`;
            }
            
            document.getElementsByClassName("page")[0].classList.add("active");                                             // добавляем класс 1 кнопке для подсветки активной страницы в пагинации

        }

        function createTable() {                                                                                            // функция создания таблицы
            console.log('Вызов метода построения таблицы');                                                                 
            let table = document.getElementById("table-view");                                                              // задаём контейнер хранения таблицы

            let start = end =  0;                                                                                           // создаём переменные для хранения начального и конечного элемента в массиве 

            if(tableData.length <= tableStrings) {                                                                          // проверяем таблицу на кол-во элементов, если таблица пуста или данных в ней меньше заданного кол-ва,
                start = 0;                                                                                                  // то начинаем с 0 и заканчиваем кол-вом элементов в таблице
                end = tableData.length - 1;
            } else {                                                                                                        // если данных много, то определяем начальный и последний элемент в таблице по активному индексу страницы
                end = activePage * tableStrings - 1;
                start = (activePage * tableStrings) - tableStrings;
            }

            
            console.log("Начальный элемент таблицы: ", start);
            console.log("Конечный элемент таблицы: ", end);

            table.innerHTML = "";                                                                                           // очищаем контейнер с таблицей

            for(let i = start; i <= end; i++) {                                                                             // в цикле перебираем элементы массива и выводим данные в таблицу
                table.innerHTML += `
                    <tr>
                        <td>${tableData[i].fullname}</td>
                        <td>${tableData[i].position_name}</td>
                        <td>${tableData[i].user_hired}</td>
                        <td>${tableData[i].user_fired == null ? '': tableData[i].user_fired}</td>
                        <td>${tableData[i].user_fired_reason == null ? '': tableData[i].user_fired_reason}</td>
                        <td>${tableData[i].user_salary}</td>
                        <td>${tableData[i].leader_fullname}</td>
                    </tr>
                `
            }
            console.log("Конец метода построения таблицы");
        }

        function setActivePage(index) {                                                                                     // функция переключения страниц
            console.log("Выбранный индекс для активной страницы: ", index);
            if(index == activePage) return;                                                                                 // если выбранный индекс не отличается от активного индекса, то выходим из метода
            let oldActivePage = document.getElementsByClassName("active")[0].classList.remove("active");                    // находим сттарый индекс для удаления класса подсветки
            let newActivePage = document.getElementsByClassName("page")[index-1].classList.add("active");                   // находим кнопку с выбранным индексом и добавляем класс для подсветки
            activePage = index;                                                                                             // устанавливаем активный индекс

            createTable();                                                                                                  // вызываем метод построения таблицы с данными по выбранной странице
        }


        function sendAjax(filterType = 0) {                                                                                 // функция получения данных по фильтрам

            console.log('Вызов метода получения данных по фильтру: ', filterType);

            $.ajax({                                                                                                        // создаём запрос на получение данных
                url: 'users.php/get-filtered-data',                                                                         // название метода на бэке
                type: 'post',
                dataType: 'json',
                data: {
                    'filter': filterType                                                                                    // выбранный фильтр
                },
                error: (err) => {                                                                                           // обработка ошибок
                    console.error("При выполнении запроса произошла ошибка: ", err.responseText);
                    tableData = [];
                },
                success: (response) => {                                                                                    // обработка полученных данных
                    console.info("Полученный ответ: ", response);

                    if(response.length) {

                        response.sort((a,b) => {                                                                            // сортировка полученного массива по ФИО
                        if (a.fullname > b.fullname) {
                                return 1;
                            } else if (a.fullname < b.fullname) {
                                return -1;
                            }
                            return 0;
                        });

                        tableData = response;
                    }
                                              
                    let pageCount = response.length && response.length >= 10 ? response.length / tableStrings: 1;           // получаем количество страниц
                    
                    createPagination(pageCount);                                                                            // вызываем метод создания пагинации

                    createTable();                                                                                          // вызываем метод создания таблицы
                }
            });

        }

        // добавляем прослушку события для определения загрузки страницы и получения первоначальных данных
        window.addEventListener('load', function() {
            console.info('Страница загружена');
            sendAjax();                                                                                                     // вызываем метод для получения начальных данных
        });
    </script>
</body>
</html>
